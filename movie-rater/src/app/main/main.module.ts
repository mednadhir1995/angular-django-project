import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {Routes,RouterModule} from '@angular/router';
import { MainComponent } from './main.component';
import { from } from 'rxjs';
import{ApiService} from '../api.service';
import { MovieListComponent } from './movie-list/movie-list.component';
import { MovieDetailComponent } from './movie-detail/movie-detail.component';
import { MovieFromComponent } from './movie-from/movie-from.component';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import {ReactiveFormsModule} from'@angular/forms';

const routes:Routes=[
  {path:'movies',component:MainComponent}
];


@NgModule({
  declarations: [MainComponent
  ,
  MovieListComponent,
  MovieDetailComponent,
  MovieFromComponent],
  
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    AngularFontAwesomeModule,
    ReactiveFormsModule,
  ],
  exports:[
    RouterModule
  ],
  providers:[
    ApiService
  ]
})
export class MainModule { }
