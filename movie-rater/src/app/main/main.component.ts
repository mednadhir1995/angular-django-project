import { Component, OnInit } from '@angular/core';
 import { Movie } from '../models/movie';
 import{ApiService} from '../api.service';
 import {CookieService} from'ngx-cookie-service';
 import {Router} from '@angular/router';
@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {
  movies:any=[];
  selectectedMovie=null;
  editedMovie=null;
  constructor(private apiService:ApiService,private cookieService:CookieService, private router:Router) { }

  ngOnInit() {
    const mrToken = this.cookieService.get("mr-token");
      if(!mrToken){
        this.router.navigate(['/auth']);
        }else{     this.apiService.getMovies().subscribe(
          (data:Movie[])=>{
            this.movies=data;
          },
          error=>{
            console.log(error);
          }
        );
        }
    
  }

  logout(){
    this.cookieService.delete('mr-token');
    this.router.navigate(['/auth']);
  }

  
  selectMovie(movie:Movie){
    this.selectectedMovie=movie;
    this.editedMovie=null;

    console.log("selected Movie:",this.selectectedMovie);
  }

  editMovie(movie:Movie){
    this.editedMovie=movie;
    this.selectectedMovie=null;

    console.log("edited Movie:",this.editedMovie);
  }
  createNewMovie(){
    this.editedMovie={title:'',description:''};
    this.selectectedMovie=null;

  }
  deletedMovie(movie:Movie){
    this.apiService.deleteeMovie(movie.id).subscribe(
      data=>{
this.movies=this.movies.filter(mov=>mov.id !==movie.id) ;     },
      error=>{
        console.log(error);
      }
    )

  
    console.log("delete a movie", movie.title);
  }
  movieCreated(movie:Movie){
this.movies.push(movie);
this.editedMovie=null;

  }
  movieUpdated(movie:Movie){
   const index = this.movies.findIndex(mov=>mov.id=== movie.id);
   if (index >=0){
    this.movies[index]=movie;
   }
   this.editedMovie=null;

  }
}
