import { Component, OnInit,Input,Output,EventEmitter } from '@angular/core';
import{ApiService} from '../../api.service';
import { Movie } from '../../models/movie';
@Component({
  selector: 'app-movie-detail',
  templateUrl: './movie-detail.component.html',
  styleUrls: ['./movie-detail.component.css']
})
export class MovieDetailComponent implements OnInit {

@Input() movie :Movie;
rateHovered=0;
@Output() updateMovie = new EventEmitter<Movie>();
  constructor(private apiService:ApiService ) { }

  ngOnInit() {
  }
  rateHover(rate:number){
    this.rateHovered=rate ;
  }
  rateClicked(rate:number){
this.apiService.rateMovie(rate,this.movie.id).subscribe(
  data=>
   this.getDetails(),
   error=>console.log(error)

);
  }
  
  getDetails(){
    this.apiService.getMovie(this.movie.id).subscribe(
      (movie:Movie)=>{
        this.updateMovie.emit(movie);
      },error=>console.log(error)
    
    );
      }

}
