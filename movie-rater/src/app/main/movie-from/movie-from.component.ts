import { Component, OnInit,Input,Output,EventEmitter } from '@angular/core';
import { Movie } from '../../models/Movie';
import{ FormGroup,FormControl} from'@angular/forms';

import{ApiService} from '../../api.service';

@Component({
  selector: 'app-movie-from',
  templateUrl: './movie-from.component.html',
  styleUrls: ['./movie-from.component.css']
})
export class MovieFromComponent implements OnInit {
movieForm;
id=null;

@Output() movieCreated=new EventEmitter<Movie>();
@Output() movieUpdated=new EventEmitter<Movie>();


@Input()  set movie(value:Movie){
  this.id=value.id;
  this.movieForm= new FormGroup({
    title:new FormControl(value.title),
    description : new FormControl(value.description)
  });
}




  constructor(private apiService:ApiService) { }

  ngOnInit() {
  }

  formDisabled(){
    if(this.movieForm.value.title.length &&
      this.movieForm.value.description.length ){
return false;
    }else{ return true;}

  }





  saveForm(){
   if(this.id){
    this.apiService.updateMovie(this.id,this.movieForm.value.title,this.movieForm.value.description).subscribe(
      (result:Movie)=>this.movieUpdated.emit(result),
      err=>console.log(err)

    );
  }else{
    this.apiService.createMovie(this.movieForm.value.title,this.movieForm.value.description).subscribe(
      (result:Movie)=>this.movieCreated.emit(result),
      err=>console.log(err)

    );
  }
  }


   }




